package com.example.ak.words.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.ak.words.R;
import com.example.ak.words.model.Training;

/**
 * Training result
 */
public class TrainingResultFragment extends StartTrainingFragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_training_result, container, false);

        Button startButton = (Button) view.findViewById(R.id.button_start_training);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNewTraining();
            }
        });

        TextView resultTextView = (TextView) view.findViewById(R.id.text_result);
        resultTextView.setText(String.format("%d/%d", Training.getInstance().getCorrectAnswersCount(),
                Training.getInstance().size()));

        return view;
    }
}
