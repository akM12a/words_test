package com.example.ak.words.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.MenuItem;

import com.example.ak.words.NavigationListener;
import com.example.ak.words.R;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * All app fragments extends this class
 */
public abstract class BaseAppFragment extends Fragment {

    protected interface ApiRunnable<T> {
        void run(Callback<T> callback);
    }

    protected interface CompleteListener<T> {
        void onComplete(T result);
    }

    protected NavigationListener mNavigationListener;
    private ProgressDialog mProgressDialog;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof NavigationListener) {
            mNavigationListener = (NavigationListener) context;
        }
        else {
            throw new IllegalStateException("Activity not implement NavigationListener");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void showProgress(boolean visible) {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
        if (visible) {
            mProgressDialog = ProgressDialog.show(getContext(), null, getString(R.string.loading));
        }
    }

    private <T> void handleServiceError(Exception error, final ApiRunnable<T> request,
                                        final CompleteListener<T> listener) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity())
                .setMessage(R.string.data_loading_error)
                .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        executeApiRequest(request, listener);
                    }
                })
                .setNegativeButton(android.R.string.cancel, null);
        alertDialog.show();
    }

    protected <T> void executeApiRequest(final ApiRunnable<T> request,
                                         final CompleteListener<T> listener) {
        showProgress(true);

        request.run(new Callback<T>() {
            @Override
            public void success(final T result, Response response) {
                showProgress(false);
                if (listener != null) {
                    if (!isRemoving() && !isDetached() && isAdded()) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                listener.onComplete(result);
                            }
                        });
                    }
                }
            }

            @Override
            public void failure(final RetrofitError error) {
                showProgress(false);
                if (!isRemoving() && !isDetached() && isAdded()) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            handleServiceError(error, request, listener);
                        }
                    });
                }
            }
        });
    }
}
