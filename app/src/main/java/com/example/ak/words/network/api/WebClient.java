package com.example.ak.words.network.api;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

public class WebClient {
    private static final String BASE_URL = "http://dictionary.skyeng.ru/api/v1";
    private static final int CONNECT_TIMEOUT = 10;
    private static final int READ_WRITE_TIMEOUT = 10;

    private static Api mApi;

    static {
        Executor executor = Executors.newSingleThreadExecutor();
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .setClient(new OkClient(getHttpClient()))
                .setExecutors(executor, executor)
                .setConverter(new GsonConverter(new Gson()))
                .build();
        mApi = restAdapter.create(Api.class);
    }

    public static Api getApi() {
        return mApi;
    }

    private static OkHttpClient getHttpClient() {
        OkHttpClient client = new OkHttpClient();

        client.setConnectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS);
        client.setReadTimeout(READ_WRITE_TIMEOUT, TimeUnit.SECONDS);
        client.setWriteTimeout(READ_WRITE_TIMEOUT, TimeUnit.SECONDS);

        return client;
    }
}
