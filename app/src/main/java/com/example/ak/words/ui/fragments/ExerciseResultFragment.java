package com.example.ak.words.ui.fragments;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ak.words.R;
import com.example.ak.words.model.Training;
import com.example.ak.words.model.Word;
import com.example.ak.words.ui.BaseAppFragment;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Exercise result
 */
public class ExerciseResultFragment extends BaseAppFragment {

    TextView mWordText;
    TextView mTranslationText;
    ImageView mArtImage;
    Button mNextButton;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_exercise_result, container, false);

        mWordText = (TextView) view.findViewById(R.id.text_word);
        mTranslationText = (TextView) view.findViewById(R.id.text_translation);
        mArtImage = (ImageView) view.findViewById(R.id.image_art);
        mNextButton = (Button) view.findViewById(R.id.button_next);

        Word word = Training.getInstance().getCurrentWord();
        mWordText.setText(word.getWord());
        mTranslationText.setText(word.getTranslation());

        mTranslationText.setVisibility(View.INVISIBLE);

        Picasso.with(getContext())
                .load("https:" + word.getImageUrl())
                .into(mArtImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        mTranslationText.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError() {
                        mTranslationText.setVisibility(View.VISIBLE);
                    }
                });

        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Training.getInstance().next();
                if (Training.getInstance().isFinished()) {
                    mNavigationListener.showTrainingResult();
                }
                else {
                    mNavigationListener.showCurrentExercise();
                }
            }
        });

        return view;
    }

}
