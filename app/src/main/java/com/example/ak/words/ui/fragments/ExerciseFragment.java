package com.example.ak.words.ui.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ak.words.R;
import com.example.ak.words.model.AlternativeWord;
import com.example.ak.words.model.Training;
import com.example.ak.words.model.Word;
import com.example.ak.words.ui.BaseAppFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Choose alternative fragment
 */
public class ExerciseFragment extends BaseAppFragment {

    private static final int VARIANTS_COUNT = 3;

    LinearLayout mLayoutVariants;
    TextView mWordText;
    Button mSkipButton;
    boolean mAnswered = false;

    private View createVariantView(final String text, final boolean isCorrect, int layout) {
        final View variantView = View.inflate(getContext(), layout, null);
        final Button variantButton = (Button) variantView.findViewById(R.id.button_variant);
        variantButton.setText(text);
        variantButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAnswered) return;

                mAnswered = true;

                if (isCorrect) {
                    Training.getInstance().correct();
                }
                else {
                    Training.getInstance().incorrect();
                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mNavigationListener.showCurrentExerciseResult();
                    }
                }, 1000);

                variantButton.setBackgroundResource(isCorrect ? R.drawable.variant_green : R.drawable.variant_red);
                variantButton.setTextColor(Color.WHITE);
            }
        });
        return variantView;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_exercise, container, false);

        mWordText = (TextView) view.findViewById(R.id.text_word);
        mLayoutVariants = (LinearLayout) view.findViewById(R.id.layout_variants);
        mSkipButton = (Button) view.findViewById(R.id.button_skip);

        Word word = Training.getInstance().getCurrentWord();
        mWordText.setText(word.getTranslation());

        List<AlternativeWord> copy = new ArrayList<>(word.getAlternativeWords());
        Collections.shuffle(copy);
        final List<AlternativeWord> alternatives = copy.subList(0, (copy.size() > VARIANTS_COUNT) ?
                VARIANTS_COUNT : copy.size());

        for (int i=0; i<alternatives.size(); ++i) {
            final View variantView = createVariantView(alternatives.get(i).getWord(), false, R.layout.item_variant_white);
            mLayoutVariants.addView(variantView);
        }

        View variantView = createVariantView(word.getWord(), true, R.layout.item_variant_white);
        mLayoutVariants.addView(variantView, new Random().nextInt(alternatives.size()+1));

        mSkipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAnswered) return;
                Training.getInstance().incorrect();
                mNavigationListener.showCurrentExerciseResult();
            }
        });

        return view;
    }
}
