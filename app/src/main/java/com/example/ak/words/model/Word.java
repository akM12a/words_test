package com.example.ak.words.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Word model
 */
public class Word {
    @SerializedName("text") private String word;
    @SerializedName("translation") private String translation;
    @SerializedName("images") private List<String> imageUrls;
    @SerializedName("alternatives") private List<AlternativeWord> alternativeWords;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    public List<AlternativeWord> getAlternativeWords() {
        return alternativeWords;
    }

    public void setAlternativeWords(List<AlternativeWord> alternativeWords) {
        this.alternativeWords = alternativeWords;
    }

    public List<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(List<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public String getImageUrl() {
        return (imageUrls.size() > 0)? imageUrls.get(0): null;
    }
}