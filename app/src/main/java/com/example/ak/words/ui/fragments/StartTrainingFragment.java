package com.example.ak.words.ui.fragments;

import android.graphics.Point;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.ak.words.R;
import com.example.ak.words.model.Training;
import com.example.ak.words.model.Word;
import com.example.ak.words.network.api.WebClient;
import com.example.ak.words.ui.BaseAppFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import retrofit.Callback;

/**
 * Start training fragment
 */
public class StartTrainingFragment extends BaseAppFragment {

    private static final Integer WORDS_IN_TRAINING = 10;
    private static final List<Integer> WORD_ID_LIST = Arrays.asList(211138, 226138, 177344, 196957,
            224324, 89785, 79639, 173148, 136709, 158582, 92590, 135793, 68068, 64441, 46290,
            128173, 51254, 55112, 222435);

    protected void startNewTraining() {
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
        List<Integer> copy = new ArrayList<>(WORD_ID_LIST);
        Collections.shuffle(copy);
        final List<Integer> trainingIds = copy.subList(0, (copy.size() > WORDS_IN_TRAINING)?
                WORDS_IN_TRAINING: copy.size());
        executeApiRequest(new ApiRunnable<List<Word>>() {
            @Override
            public void run(Callback<List<Word>> callback) {
                WebClient.getApi().getTranslation(TextUtils.join(",", trainingIds), size.x, callback);
            }
        }, new CompleteListener<List<Word>>() {
            @Override
            public void onComplete(List<Word> result) {
                Training.getInstance().startNewTraining(result);
                mNavigationListener.showCurrentExercise();
            }
        });
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_start_training, container, false);

        Button startButton = (Button) view.findViewById(R.id.button_start_training);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNewTraining();
            }
        });

        return view;
    }

}
