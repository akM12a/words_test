package com.example.ak.words.model;

import com.google.gson.annotations.SerializedName;

/**
 * Alternative word model
 */
public class AlternativeWord {
    @SerializedName("text") private String word;
    @SerializedName("translation") private String translation;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }
}
