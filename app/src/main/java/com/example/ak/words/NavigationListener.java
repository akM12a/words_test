package com.example.ak.words;

/**
 * App navigation
 */
public interface NavigationListener {
    void showCurrentExercise();
    void showCurrentExerciseResult();
    void showTrainingResult();
}
