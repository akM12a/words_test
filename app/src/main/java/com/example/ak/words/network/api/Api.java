package com.example.ak.words.network.api;

import com.example.ak.words.model.Word;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * API
 */
public interface Api {
    @GET("/wordtasks")
    void getTranslation(@Query("meaningIds") String wordIdList, @Query("width") int width, Callback<List<Word>> callback);
}