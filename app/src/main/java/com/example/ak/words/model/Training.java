package com.example.ak.words.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Training model
 */
public class Training {
    private List<Word> words;
    private boolean answers[];
    private int currentExercise;

    private static class Holder {
        private static Training instance = new Training();
    }

    public static Training getInstance() {
        return Holder.instance;
    }

    private Training() {
        words = new ArrayList<>();
        currentExercise = 0;
    }

    public void startNewTraining(List<Word> words) {
        this.words.clear();
        this.words.addAll(words);
        this.answers = new boolean[words.size()];
        currentExercise = 0;
    }

    public boolean isFinished() {
        return currentExercise >= words.size();
    }

    public Word getCurrentWord() {
        return (!isFinished())? words.get(currentExercise): null;
    }

    public void next() {
        ++currentExercise;
    }

    public int size() {
        return words.size();
    }

    public int getCurrentExercise() {
        return currentExercise;
    }

    public void correct() {
        answers[currentExercise] = true;
    }

    public void incorrect() {
        answers[currentExercise] = false;
    }

    public int getCorrectAnswersCount() {
        int count = 0;
        for (int i=0;i<answers.length; ++i)
            count += answers[i]? 1: 0;
        return count;
    }
}
