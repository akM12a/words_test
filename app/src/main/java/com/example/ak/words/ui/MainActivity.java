package com.example.ak.words.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.example.ak.words.NavigationListener;
import com.example.ak.words.R;
import com.example.ak.words.model.Training;
import com.example.ak.words.model.Word;
import com.example.ak.words.ui.fragments.ExerciseFragment;
import com.example.ak.words.ui.fragments.ExerciseResultFragment;
import com.example.ak.words.ui.fragments.StartTrainingFragment;
import com.example.ak.words.ui.fragments.TrainingResultFragment;

import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationListener {

    ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mProgressBar = (ProgressBar) findViewById(R.id.progress_training);

        StartTrainingFragment wordsFragment = new StartTrainingFragment();
        setContent(wordsFragment, false);
    }

    private void setContent(BaseAppFragment fragment, boolean animated) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if (animated) {
            ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right,
                    android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        }
        ft.replace(R.id.content_frame, fragment);
        ft.commit();
    }

    private void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }

    private void showProgress() {
        mProgressBar.setMax(Training.getInstance().size());
        mProgressBar.setProgress(Training.getInstance().getCurrentExercise());
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void showCurrentExercise() {
        showProgress();
        ExerciseFragment fragment = new ExerciseFragment();
        setContent(fragment, true);
    }

    @Override
    public void showCurrentExerciseResult() {
        showProgress();
        ExerciseResultFragment fragment = new ExerciseResultFragment();
        setContent(fragment, true);
    }

    @Override
    public void showTrainingResult() {
        hideProgress();
        TrainingResultFragment fragment = new TrainingResultFragment();
        setContent(fragment, true);
    }
}
